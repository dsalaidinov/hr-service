const request = require('supertest');
const app = require('../src/server/index.js');

describe('Post Endpoints', () => {
  it('should create a new employee', async () => {
    const res = await request(app).post('/api/staff').send({
      birthDate: '1990-04-01T10:20:27.599Z',
      email: 'test@mail.ru',
      firstName: 'Мария',
      joinDate: '2022-01-10T13:20:27.599Z',
      lastName: 'Петрова',
      phone: '+996509123456',
      role: 'manager',
      salary: 50000,
    });
    expect(res.statusCode).toEqual(201);
  });
  it('should fetch a single employee', async () => {
    const employeeId = 9;
    const res = await request(app).get(`/api/staff/${employeeId}`);

    expect(res.statusCode).toEqual(200);
    expect(res.body).toMatchObject({
      birthDate: '1990-04-01T10:20:27.599Z',
      createdAt: '2022-04-13T07:37:53.081Z',
      email: 'test@mail.ru',
      firstName: 'Мария',
      id: 9,
      joinDate: '2022-01-10T13:20:27.599Z',
      lastName: 'Петрова',
      phone: '+996509123456',
      role: 'manager',
      salary: 50000,
      updatedAt: '2022-04-13T07:37:53.081Z',
    });
  });
  it('should fetch all employees', async () => {
    const res = await request(app).get('/api/staff/');
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(1);
  });
  it('should update a employee', async () => {
    const res = await request(app).put('/api/staff/9').send({
      firstName: 'Никита',
    });
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({ message: 'Employee was updated successfully.' });
  });
  it('should return status code 500 if db constraint is violated', async () => {
    const res = await request(app).post('/api/staff').send({
      firstName: 'te',
    });
    expect(res.statusCode).toEqual(500);
    expect(res.body).toHaveProperty('error');
  });
  it('should delete a employee', async () => {
    const res = await request(app).delete('/api/staff/1');
    expect(res.statusCode).toEqual(204);
  });
  it('should respond with status code 404 if resource is not found', async () => {
    const employeeId = 1;
    const res = await request(app).get(`/api/staff/${employeeId}`);
    expect(res.statusCode).toEqual(404);
  });
});
