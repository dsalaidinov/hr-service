// Simple usage of callback
function greeting(name) {
  console.log('Hello ' + name);
}

function processUserInput(callback) {
  var name = prompt('Please enter your name.');
  callback(name);
}

processUserInput(greeting);

/* 
    Rewrite using async/await
  */

async function greeting(name) {
  return (greet = await Promise.resolve(`Hello, ${name}!`));
}

greeting(prompt('Please enter your name.')).then(console.log);
