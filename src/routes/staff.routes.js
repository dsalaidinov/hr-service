module.exports = (app) => {
  const router = require('express').Router();
  const staff = require('../network/controllers/staff_controller');

  // Add a new employee
  router.post('/', staff.create);
  // Retrieve all employees
  router.get('/', staff.findAll);
  // Retrieve a single employee with id
  router.get('/:id', staff.findOne);
  //   // Update a employee with id
  router.put('/:id', staff.update);
  //   // Delete a employee with id
  router.delete('/:id', staff.deleteEmployee);

  app.use('/api/staff', router);
};
