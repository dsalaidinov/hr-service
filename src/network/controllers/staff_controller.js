const db = require('../../services/staff_service');
const Staff = db.staff;

function findAll(req, res) {
  const filters = req.query;

  Staff.findAll({ where: filters })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while retrieving employees.',
      });
    });
}

function create(req, res) {
  // Validate request
  if (!req.body.firstName || !req.body.lastName) {
    res.status(400).send({
      message: 'Content can not be empty!',
    });

    return;
  }
  // Create a employee
  const staff = {
    birthDate: req.body.birthDate,
    email: req.body.email,
    firstName: req.body.firstName,
    joinDate: req.body.joinDate,
    lastName: req.body.lastName,
    phone: req.body.phone,
    role: req.body.role,
    salary: req.body.salary,
  };
  // Save employee in the database
  Staff.create(staff)
    .then((data) => {
      res.status(201).send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || 'Some error occurred while creating the employee.',
      });
    });
}

function findOne(req, res) {
  const id = req.params.id;

  Staff.findByPk(id)
    .then((data) => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Cannot find employee with id=${id}.`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: 'Error retrieving employee with id=' + id,
      });
    });
}

function update(req, res) {
  const id = req.params.id;

  Staff.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: 'Employee was updated successfully.',
        });
      } else {
        res.send({
          message: `Cannot update employee with id=${id}. Maybe employee was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: 'Error updating employee with id=' + id,
      });
    });
}

function deleteEmployee(req, res) {
  const id = req.params.id;

  Staff.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: 'Employee was deleted successfully!',
        });
      } else {
        res.send({
          message: `Cannot delete Employee with id=${id}. Maybe Employee was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: 'Could not delete Employee with id=' + id,
      });
    });
}

module.exports = {
  create,
  deleteEmployee,
  findAll,
  findOne,
  update,
};
