module.exports = (sequelize, DataTypes) => {
  const Staff = sequelize.define('staff', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    firstName: {
      type: DataTypes.STRING,
    },
    lastName: {
      type: DataTypes.STRING,
    },
    role: {
      type: DataTypes.STRING,
    },
    birthDate: {
      type: DataTypes.DATE,
    },
    joinDate: {
      type: DataTypes.DATE,
    },
    salary: {
      type: DataTypes.FLOAT,
    },
    email: {
      type: DataTypes.STRING,
    },
    phone: {
      type: DataTypes.STRING,
    },
  });

  return Staff;
};
