const express = require('express');

const server = express();
server.use(express.json());
server.use(express.urlencoded({ extended: true }));

const db = require('../services/staff_service');
db.sequelize.sync();

require('../routes/staff.routes')(server);

server.get('/', (req, res) => {
  res.json({ message: 'Welcome to the HR service' });
});

module.exports = server;
