// dependencies
const Sequelize = require('sequelize');
jest.mock('sequelize');

describe('database wrapper', () => {
  beforeEach(() => {
    Sequelize.mockClear();
  });

  it('exports an instance of sequelize', () => {
    expect(Sequelize).not.toHaveBeenCalled();
    require('../staff_service');
    expect(Sequelize).toHaveBeenCalledWith('hr', 'postgres', '', {
      dialect: 'postgres',
      host: 'localhost',
      operatorsAliases: false,
      pool: {
        acquire: 30000,
        idle: 10000,
        max: 5,
        min: 0,
      },
    });
  });
});
